package nl.saxion.RecipeApp.ui;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.List;

import nl.saxion.RecipeApp.R;
import nl.saxion.RecipeApp.data.RecipeIngredient;

public class ValueGraph extends View {

    public float carbs;
    public float proteins;
    public float fiber;
    public float fats;
    public float total;
    private float width = 400;
    private Paint carbsPaint;
    private Paint proteinPaint;
    private Paint fiberPaint;
    private Paint fatPaint;
    private float left = 80;
    private Paint textPaint;

    /**
     * It calculates summary of carbs, proteins, fibers and fats.
     * Sets paints for each individual part.
     * @param graphIngredients ingredients selected and saved in recipe.
     */
    public void setIngredients(List<RecipeIngredient> graphIngredients){

        for (int i = 0; graphIngredients.size()>i; i++){
            RecipeIngredient aName = graphIngredients.get(i);
            carbs += (aName.ingredient.carbs * (aName.weight/100f));
            proteins += (aName.ingredient.proteins * (aName.weight/100f));
            fiber += (aName.ingredient.fiber * (aName.weight/100f));
            fats += (aName.ingredient.fats * (aName.weight/100f));
        }
        total = carbs + proteins + fats + fiber;
        carbsPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        carbsPaint.setColor(0xFFFF0000);

        proteinPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        proteinPaint.setColor(0xFF00FF00);

        fiberPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fiberPaint.setColor(0xFF0000FF);

        fatPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fatPaint.setColor(0xFFFF00FF);

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(0xFF000000);
        textPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.graphFontSize));
    }


    public ValueGraph(Context context) {
        super(context);
    }

    public ValueGraph(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ValueGraph(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Implemented to save the actual width of our view to match bars with it.
     * @param w is the width of the view.
     * @param h is height of the view.
     * @param oldw is the old width of the view which is not used.
     * @param oldh is the old height of the view which is also not used.
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width=w;
    }

    /**
     * Draws a bar graph for carbs, proteins, fats and fibers.
     * @param canvas is provided by android.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float carbsTotal = carbs/total;
        float proteinsTotal = proteins/total;
        float fatsTotal = fats/total;
        float fiberTotal= fiber/total;
        canvas.drawRect(left,0f, carbsTotal*width + left, 50f,carbsPaint);
        canvas.drawRect(left,80f, proteinsTotal*width + left, 130f,proteinPaint);
        canvas.drawRect(left,160f, fatsTotal*width + left, 210f,fatPaint);
        canvas.drawRect(left,240f, fiberTotal*width + left, 290f,fiberPaint);
        canvas.drawText("Carbs " + carbs + " g.", left,350f, textPaint);
        canvas.drawText("Proteins " + proteins + " g.", left,430f, textPaint);
        canvas.drawText("Fats " + fats + " g.", left,510f, textPaint);
        canvas.drawText("Fibers " + fiber + " g.", left,590f, textPaint);



    }

    public ValueGraph(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
