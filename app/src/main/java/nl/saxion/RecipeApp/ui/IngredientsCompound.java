package nl.saxion.RecipeApp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.List;

import nl.saxion.RecipeApp.data.Ingredient;
import nl.saxion.RecipeApp.data.RecipeIngredient;
import nl.saxion.RecipeApp.ui.main.IngredientSpinnerAdapter;
import nl.saxion.RecipeApp.ui.main.SelectedIngredientAdapter;

/**
 * The class to make a spinner and a list of ingredients to choose from.
 * With the spinner you can select a new ingredient.
 * In the list you can change the weight and remove selected ingredient.
 */
public class IngredientsCompound extends LinearLayout {


    private List<RecipeIngredient> selectedIngredient;
    private SelectedIngredientAdapter herpy;
    private SearchableSpinner searchableSpinnerCompound;
    private RecyclerView recyclerViewCompound;
    private int recipeID;


    private void addViews(AttributeSet attrs){
        searchableSpinnerCompound = new SearchableSpinner(getContext(), attrs);
        recyclerViewCompound = new RecyclerView(getContext());

        searchableSpinnerCompound.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        recyclerViewCompound.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 300));
        recyclerViewCompound.setLayoutManager(new LinearLayoutManager(getContext()));

        this.addView(searchableSpinnerCompound);
        this.addView(recyclerViewCompound);

        searchableSpinnerCompound.setTitle("Select Item");
        searchableSpinnerCompound.setPositiveButton("OK");
        searchableSpinnerCompound.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Ingredient derpy = (Ingredient) searchableSpinnerCompound.getSelectedItem();
                RecipeIngredient derpyUpgraded = new RecipeIngredient(0,recipeID, derpy.ingredientId,0);
                derpyUpgraded.ingredient = derpy;
                selectedIngredient.add(derpyUpgraded);
                herpy.notifyDataSetChanged();
                searchableSpinnerCompound.setSelection(SearchableSpinner.NO_ITEM_SELECTED);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    /**
     * Sets recipe ID to be used in new recipe ingredients.
     * @param recipeID is the recipeID.
     */
    public void setRecipeID(int recipeID){
        this.recipeID = recipeID;
    }


    /**
     * Sets the list of ingredients to choose new ones from.
     * @param ingredients are ingredients you can choose from.
     */
    public void setIngredients(Ingredient[] ingredients){
        IngredientSpinnerAdapter ingredientSpinnerAdapter = new IngredientSpinnerAdapter(getContext(), android.R.layout.simple_list_item_1, ingredients);
        searchableSpinnerCompound.setAdapter(ingredientSpinnerAdapter);

    }


    /**
     * Sets the currently selected ingredients.
     * @param selectedIngredients are selected ingredients.
     */
    public void setSelectedIngredient(final List<RecipeIngredient> selectedIngredients){
        this.selectedIngredient = selectedIngredients;
        herpy = new SelectedIngredientAdapter(getContext(), selectedIngredients);
        herpy.setItemClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                View listItem =(View) v.getParent();
                RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) listItem.getTag();
                int position = viewHolder.getAdapterPosition();
                selectedIngredient.remove(position);
                herpy.notifyDataSetChanged();
            }
        });
        recyclerViewCompound.setAdapter(herpy);
    }

    /**
     * Gets the ingredients selected in this view.
     * @return a list of selected ingredients.
     */
    public List<RecipeIngredient> getSelectedIngredient() {
        return selectedIngredient;
    }

    public IngredientsCompound(Context context) {
        super(context);
        addViews(null);
    }

    public IngredientsCompound(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        addViews(attrs);
    }

    public IngredientsCompound(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addViews(attrs);
    }

    public IngredientsCompound(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        addViews(attrs);
    }
}
