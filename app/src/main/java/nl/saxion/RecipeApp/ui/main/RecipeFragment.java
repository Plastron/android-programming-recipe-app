package nl.saxion.RecipeApp.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import nl.saxion.RecipeApp.EditRecipeActivity;
import nl.saxion.RecipeApp.R;
import nl.saxion.RecipeApp.RecipeDetailActivity;
import nl.saxion.RecipeApp.data.Recipe_App_Repository;

/**
 * Fragment shown on recipe tab of the drawer.
 */
public class RecipeFragment extends Fragment {

    private RecipeAdapter adapter;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View homeScreen = inflater.inflate(R.layout.fragment_recipes, container, false);
        final RecyclerView recyclerView = homeScreen.findViewById(R.id.recipes);
        Recipe_App_Repository whateverYouWant = new Recipe_App_Repository(getContext());
        adapter = new RecipeAdapter(this.getContext(), whateverYouWant.myRecipes(null,null));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);


        adapter.setItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
                int position = viewHolder.getAdapterPosition();
                // viewHolder.getItemId();
                // viewHolder.getItemViewType();
                // viewHolder.itemView;
                Intent recipeDetailIntent = new Intent(RecipeFragment.this.getContext(), RecipeDetailActivity.class);
                recipeDetailIntent.putExtra("EXTRA_RECIPE_ID", viewHolder.getItemId());
                startActivity(recipeDetailIntent);

                }
        });


        Button newRecipe = homeScreen.findViewById(R.id.button_new_recipe);
        newRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newRecipeIntent = new Intent(RecipeFragment.this.getContext(), EditRecipeActivity.class);
                startActivity(newRecipeIntent);
            }
        });

        recyclerView.setAdapter(adapter);
        return homeScreen;
    }

    /**
     * Is being used to update recipes after adding one to database.
     */
    @Override
    public void onResume() {

        Recipe_App_Repository reload = new Recipe_App_Repository(getContext());
        this.adapter.setRecipes(reload.myRecipes(null, null));
        adapter.notifyDataSetChanged();
        super.onResume();
    }
}