package nl.saxion.RecipeApp.ui.main;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import nl.saxion.RecipeApp.R;
import nl.saxion.RecipeApp.data.RecipeIngredient;

/**
 * Is used in combination with the compound view to show list of selected ingredients for the recipe.
 */
public class SelectedIngredientAdapter extends RecyclerView.Adapter {

    private class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView ingredientName;
        public final EditText weight;
        public final ImageButton deleteButtanz;

        public ViewHolder(@NonNull View itemView, TextView ingredientName, EditText weight, ImageButton deleteButtanz) {
            super(itemView);
            this.ingredientName = ingredientName;
            this.weight = weight;
            this.deleteButtanz = deleteButtanz;
            itemView.setTag(this);
            deleteButtanz.setOnClickListener(onItemClickListener);
        }


    }
    private View.OnClickListener onItemClickListener;
    private Context context;
    private List<RecipeIngredient> adapterIngredients;

    public SelectedIngredientAdapter (Context context, List<RecipeIngredient> adapterIngredients){
        this.context = context;
        this.adapterIngredients = adapterIngredients;
        this.setHasStableIds(true);
    }


    @Override
    public long getItemId(int position) {
        return adapterIngredients.get(position).recipeIngredientID;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View root = inflater.inflate(R.layout.ingredients_layout, parent, false);
        final TextView ingredientName = root.findViewById(R.id.ingredientWeight);
        final EditText weight = root.findViewById(R.id.editGrams);
        final ImageButton deleteButtanz = root.findViewById(R.id.buttonDeleteIngredient);
        return new SelectedIngredientAdapter.ViewHolder(root, ingredientName, weight, deleteButtanz);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final SelectedIngredientAdapter.ViewHolder aHolder = (SelectedIngredientAdapter.ViewHolder) holder;
        final RecipeIngredient recipeIngredient = adapterIngredients.get(position);
        aHolder.ingredientName.setText(recipeIngredient.ingredient.ingredientName);
        aHolder.weight.setText(Integer.toString(recipeIngredient.weight));
        aHolder.weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()==false){
                    recipeIngredient.weight = Integer.parseInt(s.toString());
                }
            }
        });

    }

    public void setItemClickListener(View.OnClickListener clickListener) {
        onItemClickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return adapterIngredients.size();
    }


    public void setIngredients(List<RecipeIngredient> ingredients){
        this.adapterIngredients = ingredients;
    }

}