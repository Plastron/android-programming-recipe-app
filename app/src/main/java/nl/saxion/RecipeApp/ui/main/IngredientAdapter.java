package nl.saxion.RecipeApp.ui.main;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import nl.saxion.RecipeApp.R;
import nl.saxion.RecipeApp.data.Ingredient;


/**
 * Is adapted for ingredient recyclerView.
 */
public class IngredientAdapter extends RecyclerView.Adapter {

    private class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView ingredientName;

        public ViewHolder(@NonNull View itemView, TextView ingredientName) {
            super(itemView);
            this.ingredientName = ingredientName;
            itemView.setTag(this);
            itemView.setOnClickListener(onItemClickListener);
        }
    }

    private Context context;
    private Ingredient[] adapterIngredients;
    private View.OnClickListener onItemClickListener;

    public IngredientAdapter (Context context, Ingredient[] adapterIngredients){
        this.context = context;
        this.adapterIngredients = adapterIngredients;
        this.setHasStableIds(true);
    }

    /**
     * Is changed to get the database ID of the ingredient.
     * @param position is position in the array.
     * @return database ID.
     */
    @Override
    public long getItemId(int position) {
        return adapterIngredients[position].ingredientId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View root = inflater.inflate(R.layout.list_item_ingredient, parent, false);
        final TextView ingredientTitle = root.findViewById(R.id.text_ingredient_title);
        return new ViewHolder(root, ingredientTitle);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final IngredientAdapter.ViewHolder aHolder = (IngredientAdapter.ViewHolder) holder;
        final Ingredient currentIngredient = adapterIngredients[position];
        aHolder.ingredientName.setText(currentIngredient.ingredientName);

    }

    /**
     * Sets a clickListener for when an item in recyclerView is clicked.
     * @param clickListener
     */
    public void setItemClickListener(View.OnClickListener clickListener) {
        onItemClickListener = clickListener;
    }



    @Override
    public int getItemCount() {
        return adapterIngredients.length;
    }

    /**
     * Updates ingredients in the adapter.
     * @param ingredients is the new array of ingredients.
     */
    public void setIngredients(Ingredient[] ingredients){
        this.adapterIngredients = ingredients;
    }

}