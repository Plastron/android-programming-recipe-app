package nl.saxion.RecipeApp.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import nl.saxion.RecipeApp.EditIngredientActivity;
import nl.saxion.RecipeApp.R;
import nl.saxion.RecipeApp.data.Recipe_App_Repository;

/**
 * Fragment shown on ingredient tab of the drawer.
 */
public class IngredientFragment extends Fragment {

    private IngredientAdapter adapter;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View homeScreen = inflater.inflate(R.layout.fragment_ingredients, container, false);
        final RecyclerView recyclerView = homeScreen.findViewById(R.id.Ingredients);
        Recipe_App_Repository whateverYouWant = new Recipe_App_Repository(getContext());
        adapter = new IngredientAdapter(this.getContext(), whateverYouWant.myIngredients(null,null));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);


        adapter.setItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
                int position = viewHolder.getAdapterPosition();
                // viewHolder.getItemId();
                // viewHolder.getItemViewType();
                // viewHolder.itemView;
                Intent ingredientDetailIntent = new Intent(IngredientFragment.this.getContext(), EditIngredientActivity.class);
                ingredientDetailIntent.putExtra("EXTRA_INGREDIENT_ID", viewHolder.getItemId());
                startActivity(ingredientDetailIntent);

            }
        });
        Button newIngredient = homeScreen.findViewById(R.id.button_new_ingredient);
        newIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIngredientIntent = new Intent(IngredientFragment.this.getContext(), EditIngredientActivity.class);
                startActivity(newIngredientIntent);
            }
        });


        recyclerView.setAdapter(adapter);
        return homeScreen;
    }

    /**
     * Is used to update ingredients after adding one.
     */
    @Override
    public void onResume() {

        Recipe_App_Repository reload = new Recipe_App_Repository(getContext());
        this.adapter.setIngredients(reload.myIngredients(null, null));
        adapter.notifyDataSetChanged();
        super.onResume();
    }
}