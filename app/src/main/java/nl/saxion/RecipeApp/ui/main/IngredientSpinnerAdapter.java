package nl.saxion.RecipeApp.ui.main;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import androidx.annotation.NonNull;

import nl.saxion.RecipeApp.data.Ingredient;

/**
 * Is an adapter used to show all existing ingredients from the database in the ingredients spinner.
 */
public class IngredientSpinnerAdapter extends ArrayAdapter<Ingredient> implements SpinnerAdapter {


    public IngredientSpinnerAdapter(@NonNull Context context, int resource, @NonNull Ingredient[] objects) {
        super(context, resource, objects);
    }
}
