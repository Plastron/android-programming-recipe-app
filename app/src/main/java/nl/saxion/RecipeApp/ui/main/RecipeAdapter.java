package nl.saxion.RecipeApp.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import nl.saxion.RecipeApp.R;
import nl.saxion.RecipeApp.data.Recipe;

/**
 * Is adapter for recipe recyclerView.
 */
public class RecipeAdapter extends RecyclerView.Adapter {

    private class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView recipeTitle;
        public final TextView servingsCount;
        public ImageButton favourite;

        public ViewHolder(@NonNull View itemView, TextView recipeTitle, TextView servingsCount, ImageButton favourite) {
            super(itemView);
            this.recipeTitle = recipeTitle;
            this.servingsCount = servingsCount;
            this.favourite = favourite;
            itemView.setTag(this);
            itemView.setOnClickListener(onItemClickListener);
        }
    }




    private Context context;
    private Recipe[] adapterRecipes;
    private View.OnClickListener onItemClickListener;

    public RecipeAdapter (Context context, Recipe[] adapterRecipes){
        this.context = context;
        this.adapterRecipes = adapterRecipes;

        // It makes getItemId of my viewholder return the ID from database.
        this.setHasStableIds(true);
    }

    /**
     * Is used to get the database ID of the recipe.
     * @param position is position in the array.
     * @return database ID.
     */
    @Override
    public long getItemId(int position) {
        return adapterRecipes[position].itemId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View root = inflater.inflate(R.layout.list_item_recipe, parent, false);
        final TextView recipeTitle = root.findViewById(R.id.text_recipe_title);
        final TextView servingsCount = root.findViewById(R.id.servings);
        ImageButton favourite = root.findViewById(R.id.button_favourite);
        return new ViewHolder(root, recipeTitle, servingsCount, favourite);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final RecipeAdapter.ViewHolder aHolder = (RecipeAdapter.ViewHolder) holder;
        final Recipe currentRecipe = adapterRecipes[position];
        aHolder.recipeTitle.setText(currentRecipe.title);
        aHolder.servingsCount.setText(Integer.toString(currentRecipe.servings));
        aHolder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentRecipe.recipeFavourite == false) {
                    currentRecipe.recipeFavourite = true;
                    aHolder.favourite.setImageResource(R.drawable.ic_favourite_yes);
                }
                else {
                    currentRecipe.recipeFavourite = false;
                    aHolder.favourite.setImageResource(R.drawable.ic_favourite);
                }
            }
        });

    }

    /**
     * Sets a clickListener for when a item in recyclerView is clicked.
     * @param clickListener
     */
    public void setItemClickListener(View.OnClickListener clickListener) {
        onItemClickListener = clickListener;
    }



    @Override
    public int getItemCount() {
        return adapterRecipes.length;
    }

    /**
     * Updates recipes in the adapter.
     * @param recipes is the new array of recipes.
     */
    public void setRecipes(Recipe[] recipes){
        this.adapterRecipes = recipes;
    }

}
