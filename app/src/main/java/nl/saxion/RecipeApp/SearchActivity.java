package nl.saxion.RecipeApp;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import nl.saxion.RecipeApp.data.Recipe;
import nl.saxion.RecipeApp.data.Recipe_App_Repository;
import nl.saxion.RecipeApp.ui.main.RecipeAdapter;

/**
 * Is used for searching recipies containing value entered by the user.
 */
public class SearchActivity extends Activity {

    private RecipeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        handleIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    /**
     * It gets the recipes based on search query the user entered.
     * @param intent
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("Search",query);
            //use the query to search your data somehow //


            final RecyclerView recyclerView = findViewById(R.id.recipes);
            Recipe_App_Repository whateverYouWant = new Recipe_App_Repository(getBaseContext());

            Recipe[] obtainedRecipes = whateverYouWant.getSearchRecipes(query);
            if (obtainedRecipes.length>0){
                TextView emptySearch = findViewById(R.id.emptySearch);
                emptySearch.setVisibility(View.GONE);
            }

            adapter = new RecipeAdapter(this.getBaseContext(), obtainedRecipes);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this.getBaseContext());
            recyclerView.setLayoutManager(layoutManager);


            adapter.setItemClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
                    Intent recipeDetailIntent = new Intent(SearchActivity.this.getBaseContext(), RecipeDetailActivity.class);
                    recipeDetailIntent.putExtra("EXTRA_RECIPE_ID", viewHolder.getItemId());
                    startActivity(recipeDetailIntent);

                }
            });

            recyclerView.setAdapter(adapter);

        }
    }


}
