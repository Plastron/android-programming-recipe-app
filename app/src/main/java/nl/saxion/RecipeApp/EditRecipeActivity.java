package nl.saxion.RecipeApp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import nl.saxion.RecipeApp.data.Ingredient;
import nl.saxion.RecipeApp.data.Recipe;
import nl.saxion.RecipeApp.data.RecipeIngredient;
import nl.saxion.RecipeApp.data.Recipe_App_Repository;
import nl.saxion.RecipeApp.ui.IngredientsCompound;
import nl.saxion.RecipeApp.ui.main.IngredientSpinnerAdapter;
import nl.saxion.RecipeApp.ui.main.SelectedIngredientAdapter;

/**
 * Is used to edit the recipe.
 */
public class EditRecipeActivity extends AppCompatActivity {


    /**
     * We recieve recipe from database and set the values in the editable text fields. Setting clickListener for save and delete buttons.
     * @param savedInstanceState is provided by android.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final long recipeID = getIntent().getLongExtra("EXTRA_RECIPE_ID", 0);

        // In case we are not passing anything to intent, we put 0 as a default value.

        final Recipe_App_Repository savedRecipes = new Recipe_App_Repository(getBaseContext());
        Recipe recipeToBeDisplayed = savedRecipes.getRecipe(recipeID);

        final EditText editTitle = findViewById(R.id.editTitle);
        final EditText editServings = findViewById(R.id.editServings);
        final IngredientsCompound editIngredients = findViewById(R.id.editIngredients);
        final EditText editRecipe = findViewById(R.id.editRecipeText);


        editIngredients.setRecipeID((int)recipeID);
        editIngredients.setIngredients(savedRecipes.myIngredients(null,null));

        if (recipeToBeDisplayed != null) {
            getSupportActionBar().setTitle(recipeToBeDisplayed.title + "    " + recipeToBeDisplayed.servings + "p");
            editTitle.setText(recipeToBeDisplayed.title);
            editServings.setText(Integer.toString(recipeToBeDisplayed.servings));
            editRecipe.setText(recipeToBeDisplayed.recipeItself);

            editIngredients.setSelectedIngredient(savedRecipes.ingredientsUsed((int) recipeID));
        }

        else{
            editIngredients.setSelectedIngredient(new ArrayList<RecipeIngredient>());
        }


        Button saveButton = findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /* Adding a recipe to the said library, ID is assigned automatically,
                   Title and the rest I acquire from EditText fields.
                */
                Recipe goingToBeSavedRecipe = new Recipe(
                        (int)recipeID,
                        editTitle.getText().toString(),
                        Integer.parseInt(editServings.getText().toString()),
                        editRecipe.getText().toString(),
                     //   editIngredients.getText().toString(),
                        "",
                        0,0,
                        false
                );
                int savedRecipeID = savedRecipes.saveRecipe(goingToBeSavedRecipe);
                List<RecipeIngredient> recipeIngredientList = editIngredients.getSelectedIngredient();
                for (int i = 0; recipeIngredientList.size()>i; i++){
                    RecipeIngredient  recipeIngredient = recipeIngredientList.get(i);
                    recipeIngredient.recipeID = savedRecipeID;
                    savedRecipes.saveRecipeIngredient(recipeIngredient);
                }
                finish();
                // I call the finish() method to end current activity and return to main screen.
            }
        });

        //TODO Fix the status bar!
        Button deleteButton = findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedRecipes.deleteRecipe((int) recipeID);
                finish();
            }
        });
    }
}
