package nl.saxion.RecipeApp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import nl.saxion.RecipeApp.data.Ingredient;
import nl.saxion.RecipeApp.data.Recipe_App_Repository;

/**
 * Is activity for editing ingredients.
 */
public class EditIngredientActivity extends AppCompatActivity {

    /**
     * We recieve ingredients from database and set the values in the editable text fields. Setting clickListener for save and delete buttons.
     * @param savedInstanceState is provided by anroid.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ingredient);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final long ingredientID = getIntent().getLongExtra("EXTRA_INGREDIENT_ID", 0);

        // In case we are not passing anything to intent, we put 0 as a default value.

        final Recipe_App_Repository savedIngredients = new Recipe_App_Repository(getBaseContext());
        Ingredient ingredientToBeDisplayed = savedIngredients.getIngredient(ingredientID);

        final EditText ingredientName = findViewById(R.id.ingredientName);
        final EditText nutritionalValue = findViewById(R.id.nutritionalValue);
        final EditText carbs = findViewById(R.id.carbs);
        final EditText proteins = findViewById(R.id.proteins);
        final EditText fibers = findViewById(R.id.fibers);
        final EditText fats = findViewById(R.id.fats);
        if (ingredientToBeDisplayed != null) {
            getSupportActionBar().setTitle(ingredientToBeDisplayed.ingredientName);

            ingredientName.setText(ingredientToBeDisplayed.ingredientName);
            nutritionalValue.setText(Float.toString(ingredientToBeDisplayed.nutritionalValue));
            carbs.setText(Float.toString(ingredientToBeDisplayed.carbs));
            proteins.setText(Float.toString(ingredientToBeDisplayed.proteins));
            fibers.setText(Float.toString(ingredientToBeDisplayed.fiber));
            fats.setText(Float.toString(ingredientToBeDisplayed.fats));
        }

        Button saveButton = findViewById(R.id.twat);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /* Adding a recipe to the said library, ID is assigned automatically,
                   Title and the rest I acquire from EditText fields.
                */
                Ingredient goingToBeSavedIngredient = new Ingredient(
                        (int)ingredientID,
                        ingredientName.getText().toString(),
                        Float.parseFloat(nutritionalValue.getText().toString()),
                        Float.parseFloat(carbs.getText().toString()),
                        Float.parseFloat(proteins.getText().toString()),
                        Float.parseFloat(fibers.getText().toString()),
                        Float.parseFloat(fats.getText().toString())
                );
                savedIngredients.saveIngredient(goingToBeSavedIngredient);
                finish();
            }
        });

        Button deleteButton = findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedIngredients.deleteIngredient((int) ingredientID);
                finish();
            }
        });
    }
}
