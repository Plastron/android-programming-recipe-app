package nl.saxion.RecipeApp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import java.util.List;

import nl.saxion.RecipeApp.data.Recipe;
import nl.saxion.RecipeApp.data.RecipeIngredient;
import nl.saxion.RecipeApp.data.Recipe_App_Repository;
import nl.saxion.RecipeApp.ui.ValueGraph;

/**
 * Is used to display details of a recipe.
 */
public class RecipeDetailActivity extends AppCompatActivity {

    private long recipeID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Next line of code adds the button that returns us to MainActivity - Homescreen.

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recipeID = getIntent().getLongExtra("EXTRA_RECIPE_ID", 0);

        // In case we are not passing anything to intent, we put 0 as a default value.

        displayRecipe();

        /**
         * Adds edit button when viewing selected recipe.
         */
        FloatingActionButton fabulousButton = findViewById(R.id.fabulousButton);
        fabulousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recipeDetailIntent = new Intent(RecipeDetailActivity.this.getBaseContext(), EditRecipeActivity.class);
                recipeDetailIntent.putExtra("EXTRA_RECIPE_ID", recipeID);
                startActivity(recipeDetailIntent);

            }

        });
    }

    /**
     * Obtains information about the recipe containing particular ID from database and sets text fields accordingly.
     * It finishes activity if recipe has been deleted.
     */
    public void displayRecipe(){


        Recipe_App_Repository savedRecipes = new Recipe_App_Repository(getBaseContext());
        Recipe recipeToBeDisplayed = savedRecipes.getRecipe(recipeID);

        // When i deleted a recipe, app crashed because it returned null. I fixed it by adding if statement.
        if (recipeToBeDisplayed == null) {
            finish();
        }
        else {
            getSupportActionBar().setTitle(recipeToBeDisplayed.title + "    " + recipeToBeDisplayed.servings + "p");

            TextView recipeIngredients = findViewById(R.id.recipeIngredients);
            String ingredientsText = "";
            List<RecipeIngredient> recipeIngredientList = savedRecipes.ingredientsUsed((int) recipeID);
            for (int i = 0; recipeIngredientList.size()>i; i++){
                RecipeIngredient recipeIngredient = recipeIngredientList.get(i);
                ingredientsText += recipeIngredient.toString()+ "\n";
            }
            recipeIngredients.setText(ingredientsText);
            TextView recipeRecipe = findViewById(R.id.recipeRecipe);
            recipeRecipe.setText(recipeToBeDisplayed.recipeItself);
            ValueGraph graph = findViewById(R.id.graph);
            graph.setIngredients(recipeIngredientList);
        }
    }

    /**
     * Is added, to update the recipe values if they have been edited.
     */
    @Override
    public void onResume() {

        displayRecipe();
        super.onResume();
    }
}