package nl.saxion.RecipeApp.data;

import androidx.annotation.NonNull;

public class Ingredient {
    public int ingredientId;
    public String ingredientName;
    public float nutritionalValue;
    public float carbs;
    public float proteins;
    public float fiber;
    public float fats;

    public Ingredient (int ingredientId, String ingredientName, float nutritionalValue, float carbs, float proteins, float fiber, float fats){
        this.ingredientId = ingredientId;
        this.ingredientName = ingredientName;
        this.nutritionalValue = nutritionalValue;
        this.carbs = carbs;
        this.proteins = proteins;
        this.fiber = fiber;
        this.fats = fats;

    }

    @NonNull
    @Override
    public String toString() {
        return ingredientName;
    }
}
