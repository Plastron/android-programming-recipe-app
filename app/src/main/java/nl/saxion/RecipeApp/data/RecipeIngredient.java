package nl.saxion.RecipeApp.data;

import androidx.annotation.NonNull;

public class RecipeIngredient {
    public int recipeIngredientID;
    public int recipeID;
    public int ingredientID;
    public int weight;
    public Ingredient ingredient;

    @NonNull
    @Override
    public String toString() {
        return " - " + ingredient.ingredientName + " " + weight + " g.";
    }

    public RecipeIngredient(int recipeIngredientID, int recipeID, int ingredientID, int weight){
        this.recipeIngredientID = recipeIngredientID;
        this.recipeID = recipeID;
        this.ingredientID = ingredientID;
        this.weight = weight;

    }

}
