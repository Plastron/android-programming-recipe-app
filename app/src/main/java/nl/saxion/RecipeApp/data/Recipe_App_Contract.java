package nl.saxion.RecipeApp.data;

import android.provider.BaseColumns;

public final class Recipe_App_Contract {

    private Recipe_App_Contract(){}

    public static class RecipeEntry implements BaseColumns {


        // Below lines define my columns in the table "Recipe"
        public static final String TABLE_NAME = "Recipe";
        public static final String COLUMN_NAME_TITLE = "Title";
        public static final String COLUMN_NAME_SERVINGS = "Servings";
        public static final String COLUMN_NAME_RECIPE = "Recipe";
        public static final String COLUMN_NAME_INGREDIENTS = "Ingredients";
        public static final String COLUMN_NAME_RATING = "Rating";
        public static final String COLUMN_NAME_RATING_COUNT = "Rating_count";
        public static final String COLUMN_NAME_FAVOURITE = "Favourite";


        //TODO Pictures of the dish(es) need to be added
    }

    public static class IngredientEntry implements BaseColumns{

        public static final String TABLE_NAME = "Ingredient";
        public static final String COLUMN_NAME_INGREDIENT_NAME = "Ingredient_name";
        public static final String COLUMN_NAME_NUTRITIONAL_VALUE = "Nutritional_value";
        public static final String COLUMN_NAME_CARBS = "Carbs";
        public static final String COLUMN_NAME_PROTEINS = "Proteins";
        public static final String COLUMN_NAME_FIBER = "Fiber";
        public static final String COLUMN_NAME_FATS = "Fats";

    }

    public static class RecipeIngredientEntry implements BaseColumns{
        public static final String TABLE_NAME = "Recipe_Ingredient";
        public static final String COLUMN_NAME_RECIPE_ID = "Recipe_ID";
        public static final String COLUMN_NAME_INGREDIENT_ID = "Ingredient_ID";
        public static final String COLUMN_NAME_WEIGHT = "Weight";

    }









}
