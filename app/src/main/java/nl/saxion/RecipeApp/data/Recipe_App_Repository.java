package nl.saxion.RecipeApp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

/**
 *  One place where i write down my SQL statements for searching, inserting and deleting recipes and ingredients.
 */
public class Recipe_App_Repository {


    private Recipe_App_DbHelper dataBaseHelper;
    public Recipe_App_Repository(Context databaseContext) {
        dataBaseHelper = new Recipe_App_DbHelper(databaseContext);
    }

    /**
     * Gives a list of recipes in alphabetical order
     * @param selection an optional selection
     * @param selectionArgs arguments for the optional selection
     * @return a list of recipes in alphabetical order
     */
    public Recipe[] myRecipes (String selection, String[] selectionArgs){

        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_SERVINGS,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RECIPE,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_INGREDIENTS,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING_COUNT,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_FAVOURITE
        };

        String sortOrder = Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE + " ASC";

        Cursor cursor = db.query(
                Recipe_App_Contract.RecipeEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order, Ascending based by title

        );
        List <Recipe> returnRecipes = new ArrayList<>();
        while(cursor.moveToNext()) {
            Recipe recipe = new Recipe(
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry._ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_SERVINGS)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RECIPE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_INGREDIENTS)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING_COUNT)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_FAVOURITE))==1);
            returnRecipes.add(recipe);
        }
        cursor.close();
        return returnRecipes.toArray(new Recipe[0]);
    }

    /**
     * Deletes a recipe from the database by an ID
     * @param ID the recipe ID to delete
     * @return true if the recipe is deleted
     */
    public boolean deleteRecipe(int ID)
    {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        return db.delete(Recipe_App_Contract.RecipeEntry.TABLE_NAME, Recipe_App_Contract.RecipeEntry._ID + "=?", new String[]{Integer.toString(ID)}) > 0;
    }

    /**
     * Saves a recipe to the database
     * @param entry the recipe to save
     * @return the ID of the saved recipe
     */
    public int saveRecipe(Recipe entry){
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE, entry.title);
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_SERVINGS, entry.servings);
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RECIPE, entry.recipeItself);
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_INGREDIENTS, entry.ingredients);
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING, entry.rating);
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING_COUNT, entry.ratingCount);
        values.put(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_FAVOURITE, entry.recipeFavourite);

        if (entry.itemId == 0){
            return (int) db.insert(Recipe_App_Contract.RecipeEntry.TABLE_NAME, null, values);
        }
        else{
            db.update(Recipe_App_Contract.RecipeEntry.TABLE_NAME,values, Recipe_App_Contract.RecipeEntry._ID + "=?", new String[]{Integer.toString(entry.itemId)} );
            return entry.itemId;
        }

    }

    /**
     * Gets a recipe by ID
     * @param recipeID the ID of the recipe to get
     * @return The recipe
     */
    public Recipe getRecipe(long recipeID){

        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_SERVINGS,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RECIPE,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_INGREDIENTS,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING_COUNT,
                Recipe_App_Contract.RecipeEntry.COLUMN_NAME_FAVOURITE
        };

        Cursor cursor = db.query(
                Recipe_App_Contract.RecipeEntry.TABLE_NAME,
                projection,
                Recipe_App_Contract.RecipeEntry._ID + "=?",
                new String[]{Long.toString(recipeID)},
                null,
                null,
                null

        );

        /*
         * Method gives database an ID and returns all the values of that recipe with the given ID.
         */
         Recipe returnRecipe = null;
        if (cursor.moveToNext()) {
            returnRecipe = new Recipe(
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry._ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_SERVINGS)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RECIPE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_INGREDIENTS)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING_COUNT)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeEntry.COLUMN_NAME_FAVOURITE))==1);
        }
        cursor.close();
        return returnRecipe;

    }

    /**
     * Searches for a recipe that can contain anything before input and anything after the input of the user.
     * @param query is what the user typed aka input.
     * @return returns the recipe.
     */
    public Recipe[] getSearchRecipes(String query) {
        String selection = Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE + " LIKE ?";
        String[] selectionArgs = new String[] {"%" + query + "%"};

        return myRecipes(selection, selectionArgs);
    }


    /**
     * Gives a list of ingredients, is the same as with Recipe.
     */
    public Ingredient[] myIngredients (String selection, String[] selectionArgs){

        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_NUTRITIONAL_VALUE,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_CARBS,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_PROTEINS,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FIBER,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FATS
        };



        String sortOrder = Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME + " ASC";

        Cursor cursor = db.query(
                Recipe_App_Contract.IngredientEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order, Ascending based by title

        );
        List <Ingredient> returnIngredients = new ArrayList<>();
        while(cursor.moveToNext()) {
            Ingredient ingredient = new Ingredient(
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry._ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_NUTRITIONAL_VALUE)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_CARBS)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_PROTEINS)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FIBER)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FATS)));
                    returnIngredients.add(ingredient);
        }
        cursor.close();
        return returnIngredients.toArray(new Ingredient[0]);
    }

    /**
     * Deletes a ingredient from the database by an ID
     * @param ID the ingredieng ID to delete
     * @return true if the ingredient is deleted
     */
    public boolean deleteIngredient(int ID)
    {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        return db.delete(Recipe_App_Contract.IngredientEntry.TABLE_NAME, Recipe_App_Contract.IngredientEntry._ID + "=?", new String[]{Integer.toString(ID)}) > 0;
    }

    /**
     * Saves a ingredient to the database
     * @param entry the ingredient to save
     */
    public void saveIngredient(Ingredient entry){
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME, entry.ingredientName);
        values.put(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_NUTRITIONAL_VALUE, entry.nutritionalValue);
        values.put(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_CARBS, entry.carbs);
        values.put(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_PROTEINS, entry.proteins);
        values.put(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FIBER, entry.fiber);
        values.put(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FATS, entry.fats);

        if (entry.ingredientId == 0){
            long newRowId = db.insert(Recipe_App_Contract.IngredientEntry.TABLE_NAME, null, values);
        }
        else{
            db.update(Recipe_App_Contract.IngredientEntry.TABLE_NAME,values, Recipe_App_Contract.IngredientEntry._ID + "=?", new String[]{Integer.toString(entry.ingredientId)} );
        }

    }

    /**
     * Gets a ingredient by ID
     * @param ingredientID the ID of the ingredient to get
     * @return The ingredient
     */
    public Ingredient getIngredient(long ingredientID){

        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_NUTRITIONAL_VALUE,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_CARBS,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_PROTEINS,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FIBER,
                Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FATS
        };

        Cursor cursor = db.query(
                Recipe_App_Contract.IngredientEntry.TABLE_NAME,
                projection,
                Recipe_App_Contract.IngredientEntry._ID + "=?",
                new String[]{Long.toString(ingredientID)},
                null,
                null,
                null

        );

        Ingredient returnIngredient = null;
        if (cursor.moveToNext()) {
            returnIngredient = new Ingredient(
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry._ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_NUTRITIONAL_VALUE)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_CARBS)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_PROTEINS)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FIBER)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FATS)));
        }


        cursor.close();
        return returnIngredient;

    }

    /**
     * Returns a list of recipe ingredients
     * @param recipeID is used in specific recipe
     * @return the list of recipe ingredients
     */
    public List<RecipeIngredient> ingredientsUsed (int recipeID){

        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_RECIPE_ID,
                Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_INGREDIENT_ID,
                Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_WEIGHT,
        };

        Cursor cursor = db.query(
                Recipe_App_Contract.RecipeIngredientEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_RECIPE_ID + " = ? ",              // The columns for the WHERE clause
                new String[] {Integer.toString(recipeID)},          // The values for the WHERE clause
                null,                   // don't group the rows.
                null,                    // don't filter by row groups.
                null                    // no sort order.

        );
        List <RecipeIngredient> returnIngredients = new ArrayList<>();
        while(cursor.moveToNext()) {
            RecipeIngredient ingredient = new RecipeIngredient(
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeIngredientEntry._ID)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_RECIPE_ID)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_INGREDIENT_ID)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_WEIGHT)));
            returnIngredients.add(ingredient);
            ingredient.ingredient = getIngredient(ingredient.ingredientID);
        }
                cursor.close();
        return returnIngredients;
    }

    /**
     * Saves amount of each ingredient belonging to the recipe.
     * @param entry is a connection between recipe and ingredient.
     * @return ID of said ingredient.
     */
    public int saveRecipeIngredient(RecipeIngredient entry){
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_RECIPE_ID, entry.recipeID);
        values.put(Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_INGREDIENT_ID, entry.ingredientID);
        values.put(Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_WEIGHT, entry.weight);


        if (entry.recipeIngredientID == 0){
            return (int) db.insert(Recipe_App_Contract.RecipeIngredientEntry.TABLE_NAME, null, values);
        }
        else{
            db.update(Recipe_App_Contract.RecipeIngredientEntry.TABLE_NAME,values, Recipe_App_Contract.RecipeIngredientEntry._ID + "=?", new String[]{Integer.toString(entry.recipeIngredientID)} );
            return entry.recipeIngredientID;
        }

    }

}