package nl.saxion.RecipeApp.data;

public class Recipe {

    public int itemId;
    public String title;
    public int servings;
    public String recipeItself;
    public String ingredients;
    public int rating;
    public int ratingCount;
    public boolean recipeFavourite = false;


    public Recipe(int itemId, String title, int servings, String recipeItself, String ingredients, int rating, int ratingCount, boolean recipeFavourite){
        this.itemId = itemId;
        this.title = title;
        this.servings = servings;
        this.recipeItself = recipeItself;
        this.ingredients = ingredients;
        this.rating = rating;
        this.ratingCount = ratingCount;
        this.recipeFavourite = recipeFavourite;

    }


}
