package nl.saxion.RecipeApp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Recipe_App_DbHelper extends SQLiteOpenHelper {

    /**
     * SQL statement for creating recipe table.
     */
    private static final String SQL_CREATE_RECIPE_ENTRIES =
            "CREATE TABLE " + Recipe_App_Contract.RecipeEntry.TABLE_NAME + " (" +
                    Recipe_App_Contract.RecipeEntry._ID + " INTEGER PRIMARY KEY," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_TITLE + " TEXT," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_SERVINGS + " INTEGER," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RECIPE+ " TEXT," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_INGREDIENTS + " TEXT," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING + " INTEGER," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_RATING_COUNT + " INTEGER," +
                    Recipe_App_Contract.RecipeEntry.COLUMN_NAME_FAVOURITE + " INTEGER" + ")";
    /**
     * SQL statement for creating ingredients table.
     */
    private static final String SQL_CREATE_INGREDIENT_ENTRIES =
            "CREATE TABLE " + Recipe_App_Contract.IngredientEntry.TABLE_NAME + " (" +
                    Recipe_App_Contract.IngredientEntry._ID + " INTEGER PRIMARY KEY," +
                    Recipe_App_Contract.IngredientEntry.COLUMN_NAME_INGREDIENT_NAME + " TEXT," +
                    Recipe_App_Contract.IngredientEntry.COLUMN_NAME_NUTRITIONAL_VALUE + " FLOAT," +
                    Recipe_App_Contract.IngredientEntry.COLUMN_NAME_CARBS + " FLOAT," +
                    Recipe_App_Contract.IngredientEntry.COLUMN_NAME_PROTEINS + " FLOAT," +
                    Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FIBER + " FLOAT," +
                    Recipe_App_Contract.IngredientEntry.COLUMN_NAME_FATS + " FLOAT" + ")";


    /**
     * SQL statement for creating connection table between recipe and ingredients table.
     */
    private static final String SQL_CREATE_RECIPE_INGREDIENT_ENTRIES =
            "CREATE TABLE " + Recipe_App_Contract.RecipeIngredientEntry.TABLE_NAME + " (" +
                    Recipe_App_Contract.RecipeIngredientEntry._ID + " INTEGER PRIMARY KEY," +
                    Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_RECIPE_ID + " INTEGER," +
                    Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_INGREDIENT_ID + " INTEGER," +
                    Recipe_App_Contract.RecipeIngredientEntry.COLUMN_NAME_WEIGHT + " INTEGER" + ")";

    /**
     * Delete statements for named entries.
     */
    private static final String SQL_DELETE_RECIPE_ENTRIES =
            "DROP TABLE IF EXISTS " + Recipe_App_Contract.RecipeEntry.TABLE_NAME;

    private static final String SQL_DELETE_INGREDIENT_ENTRIES =
            "DROP TABLE IF EXISTS " + Recipe_App_Contract.IngredientEntry.TABLE_NAME;

    private static final String SQL_DELETE_RECIPE_INGREDIENT_ENTRIES =
            "DROP TABLE IF EXISTS " + Recipe_App_Contract.RecipeIngredientEntry.TABLE_NAME;


    /**
     * Datanase version number 4 in this case.
     */
    public static final int DATABASE_VERSION = 4;

    /**
     * File name under which databse is saved.
     */
    public static final String DATABASE_NAME = "Recipes.db";


    public Recipe_App_DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Is called by android when a new database is created.
     * @param db is SQLiteDatabase instance.
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_RECIPE_ENTRIES);
        db.execSQL(SQL_CREATE_INGREDIENT_ENTRIES);
        db.execSQL(SQL_CREATE_RECIPE_INGREDIENT_ENTRIES);
    }

    /**
     * Called by android when database version on the device is lower than defined in the code above.
     * @param db is SQLiteDatabase instance.
     * @param oldVersion is version on the device.
     * @param newVersion is version in code.
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_RECIPE_ENTRIES);
        db.execSQL(SQL_DELETE_INGREDIENT_ENTRIES);
        db.execSQL(SQL_DELETE_RECIPE_INGREDIENT_ENTRIES);
        onCreate(db);
    }

    /**
     * Called by android when database version on the device is higher than defined in the code above.
     * @param db is SQLiteDatabase instance.
     * @param oldVersion is version on the device.
     * @param newVersion is version in code.
     */
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }



}
