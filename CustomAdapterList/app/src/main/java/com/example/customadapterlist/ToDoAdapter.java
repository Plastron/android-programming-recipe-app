package com.example.customadapterlist;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class ToDoAdapter extends ArrayAdapter<ToDoItem> {

    private ArrayList<ToDoItem> dataSet;
    private Context myContext;
    private View rowLayout;

    public ToDoAdapter(Context context, int rowLayout, ArrayList<ToDoItem> dataSet){
    super(context, rowLayout, dataSet);

    this.dataSet = dataSet;
    this.myContext = context;
    this.rowLayout = rowLayout;
    }

}
